<div align="center">
<h1 align="center">
  <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/18957739/Jmol_icon_128.png" alt="Jmol">
  <br />
  Jmol
</h1>
</div>

<b>This is the snap for [Jmol](http://jmol.sourceforge.net/)</b>,
<i>a Java molecular viewer for three-dimensional chemical structures.</i> 
  Features include reading a variety of file types and output from quantum
  chemistry programs, and animation of multi-frame files and computed normal
  modes from quantum programs.  It includes with features for chemicals,
  crystals, materials and biomolecules.  Jmol might be useful for students,
  educators, and researchers in chemistry and biochemistry.
  
  File formats read by Jmol include PDB, XYZ, CIF, CML, MDL Molfile, Gaussian,
  GAMESS, MOPAC, ABINIT, ACES-II, CASTEP, Dalton and VASP. The snap works on Ubuntu,
   Fedora, Debian, and other major Linux distributions.
